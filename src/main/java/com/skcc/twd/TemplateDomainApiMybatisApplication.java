package com.skcc.twd;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
@MapperScan(basePackages = "com.skcc.twd.customer.core.persistent")
public class TemplateDomainApiMybatisApplication {

    private static final String PROPERTIES =
            "spring.config.location="
                    + "classpath:/config/application/";

    public static void main(String[] args) {
        new SpringApplicationBuilder(TemplateDomainApiMybatisApplication.class)
                .properties(PROPERTIES)
                .run(args);
    }

}
