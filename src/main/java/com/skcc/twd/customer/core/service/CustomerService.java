package com.skcc.twd.customer.core.service;

import com.skcc.twd.customer.core.entity.Customer;
import com.skcc.twd.customer.core.object.command.CustomerRequestDTO;
import com.skcc.twd.customer.core.object.query.CustomerResponseDTO;
import com.skcc.twd.customer.core.port_infra.persistent.CustomerMapper;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Slf4j
@Transactional
public class CustomerService {

    @Autowired
    CustomerMapper customerMapper;
    @Autowired
    ModelMapper modelMapper;

    public List<Customer> getAllCustomerList(){
       return customerMapper.findAll();
    }

    public CustomerResponseDTO getCustomer(String id){
        Customer optionalCustomer = customerMapper.findById(id);
        if(null==optionalCustomer){
            return null;
        }
        return modelMapper.map(optionalCustomer, CustomerResponseDTO.class);
    }

    public void insertCustomer(CustomerRequestDTO customerRequestDTO){
        customerMapper.save(modelMapper.map(customerRequestDTO, Customer.class));
    }

    public void updateCustomer(CustomerRequestDTO customerRequestDTO){
        if(null!=getCustomer(customerRequestDTO.getId())){
            customerMapper.update(modelMapper.map(customerRequestDTO,Customer.class));
        }
    }

    public void deleteCustomer(String custId){
        if(null!=getCustomer(custId)){
            customerMapper.delete(custId);
        }
    }

}
