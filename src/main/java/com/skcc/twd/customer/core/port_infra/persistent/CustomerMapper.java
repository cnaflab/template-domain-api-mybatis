package com.skcc.twd.customer.core.port_infra.persistent;

import com.skcc.twd.customer.core.entity.Customer;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface CustomerMapper {
    List<Customer> findAll();
    Customer findById(String id);
    void save(Customer customer);
    void update(Customer customer);
    void delete(String id);
}
